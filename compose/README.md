# Compose

`docker-compose` deployment of `polkadot` and monitoring services.


## Services

Following are the main services which realted to polkadot public node deployment.
`polkadot` node will be connected to a public network(`Kusama` chain). `pexporter`
will monitor the matrixes of polkadot node. `prometheus` will scrape the matrixes
from `pexporter`. The matrix can be visualized on `grafana`.

```
1. polkadot(polkadot node which connecting to a public network)
2. pexporter(prometheus exporter of polkadot)
3. prometheus
4. grafana
```

Following are the main services which related to polkadot local network deployment.
`polkadot-alice` and `polkadot-bob` deploy alice, bob network. `polkadot-ui` 
can be used to view `blocks`, `transaction` and interact with in the ledger.

```
1. polkadot-alice
2. polkadot-bob
3. polkadot-ui
```


## Config

Change `host.docker.local` field in `.env` file and `prometheus.yml` to local
machines IP or add a host entry to `/etc/hosts` file by overriding `host.docker.local`
with local machines IP. In mac-os `host.docker.local` will route to `localhost`,
so nothing to change no mac-os.

```
# example /etc/hosts file
10.4.1.104    host.docker.local
```

`POLKADOT_DATA_DIR` in `.env` file defines data directory path of polkadot. Change 
the `POLKADOT_DATA_DIR` to local machine path.


## Deployment

Following is the way to deploy public polkadot node and `grafana` monitoring services.

```
docker-compose up -d polkadot
docker-compose up -d pexporter
docker-compose up -d prometheus
docker-compose up -d grafana
```

Following is the way to depploy local polkadot network(with two nodes) and `polkadot-ui`.
Before deploying the network, please make sure to remove preivous polkadot docker 
containers(or remove all existing docker containers with `docker rm -f $(docker ps -a -q)`)

```
docker-compose -f docker-compose-local.yml up -d polkadot-alice
docker-compose -f docker-compose-local.yml up -d polkadot-bob
docker-compose -f docker-compose-local.yml up -d polkadot-ui
```


## Monitoring

Following are the monitoring endpoints of the system(use machine ip or `localhost`
as the `host`)

```
1. pexporter - http://<host>:8000
2. prometheus - https://<host>:9090
3. grafana - http://<host>:3000
4. polkadot ui - http://<host>:80
```


## Reference

1. [Polkadot](https://github.com/chevdor/polkadot)
2. [Polkadot deployer](https://github.com/w3f/polkadot-deployer)
3. [Polkadot prometheus exporter](https://github.com/mixbytes/polkadot-prometheus-exporter)
4. [Polkadot node setup](https://medium.com/@acvlls/setting-up-a-polkadot-node-the-easy-way-3a885283091f)
5. [Polkadot local node setup](https://medium.com/@wilfried.kopp/your-very-own-local-polkadot-substrate-network-in-less-than-30s-300ed7913895)
6. [Polkadot telemetry](https://telemetry.polkadot.io)
